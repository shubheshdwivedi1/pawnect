const data = [
    {
        "_id": "5d8346a94cc2e0aa17c4421c",
        "index": 0,
        "picture": "http://placehold.it/32x32",
        "age": 40,
        "name": "Freda Alvarado",
        "gender": "female",
        "company": "QUIZKA",
        "email": "fredaalvarado@quizka.com",
        "phone": "+1 (800) 448-2665",
        "address": "368 Vermont Court, Hamilton, Delaware, 2608",
        "about": "Quis non nostrud anim cillum magna duis ea ullamco dolor laboris. Nulla duis eiusmod nostrud aliqua ullamco enim amet. Eiusmod nisi esse ad eiusmod pariatur voluptate ad ea ad sunt cillum. Amet dolor id ut aute ut ea voluptate esse laboris. Labore qui laboris laborum officia eu ea. Adipisicing dolore duis consequat mollit esse.\r\n",
        "visited": false
    },
    {
        "_id": "5d8346a95de2eeaf01e480f4",
        "index": 1,
        "picture": "http://placehold.it/32x32",
        "age": 38,
        "name": "Lowery Hurst",
        "gender": "male",
        "company": "ELPRO",
        "email": "loweryhurst@elpro.com",
        "phone": "+1 (997) 464-2660",
        "address": "581 Karweg Place, Hilltop, Virginia, 3967",
        "about": "Esse consequat anim quis reprehenderit elit aliquip minim adipisicing minim officia enim aliquip. Elit magna qui non ullamco Lorem eiusmod culpa nostrud ut elit exercitation. Exercitation voluptate cupidatat Lorem aliquip sit in aliqua minim proident magna commodo eiusmod non id. Sunt proident ex sit fugiat nostrud velit enim consectetur irure consequat eu. Sint fugiat eu excepteur deserunt anim id tempor tempor sint magna est amet in. Id sunt elit et laborum consequat esse ullamco ad sint voluptate magna ea culpa.\r\n",
        "visited": false
    },
    {
        "_id": "5d8346a93d77d890a0d50db5",
        "index": 2,
        "picture": "http://placehold.it/32x32",
        "age": 37,
        "name": "Latasha Goodman",
        "gender": "female",
        "company": "COMFIRM",
        "email": "latashagoodman@comfirm.com",
        "phone": "+1 (805) 569-2232",
        "address": "413 Porter Avenue, Chase, Kentucky, 2595",
        "about": "Ea enim sit incididunt nostrud. Cupidatat aliqua incididunt proident et adipisicing Lorem excepteur cillum aliqua nostrud veniam incididunt quis. Excepteur do consectetur officia proident veniam minim proident eiusmod excepteur elit non veniam. Nulla mollit ipsum id do officia reprehenderit consectetur consequat qui ullamco minim enim.\r\n",
        "visited": false
    },
    {
        "_id": "5d8346a9a1a400e474caf403",
        "index": 3,
        "picture": "http://placehold.it/32x32",
        "age": 23,
        "name": "Williams Vincent",
        "gender": "male",
        "company": "CODAX",
        "email": "williamsvincent@codax.com",
        "phone": "+1 (957) 453-3491",
        "address": "302 Abbey Court, Reinerton, Pennsylvania, 6407",
        "about": "Amet occaecat magna et ut id sint labore. Ex adipisicing veniam velit Lorem laborum reprehenderit duis quis cillum aute dolore velit. Veniam sunt irure ut deserunt elit aliquip. Cillum aliqua consequat minim laborum irure culpa proident consectetur labore nulla sunt.\r\n",
        "visited": false
    },
    {
        "_id": "5d8346a99085916b61691f32",
        "index": 4,
        "picture": "http://placehold.it/32x32",
        "age": 26,
        "name": "Duncan Garcia",
        "gender": "male",
        "company": "ASSISTIA",
        "email": "duncangarcia@assistia.com",
        "phone": "+1 (890) 576-2568",
        "address": "942 Anthony Street, Edgar, Guam, 3863",
        "about": "Est aute anim dolor dolore ut consectetur veniam aliqua nostrud quis dolore enim pariatur mollit. Fugiat sunt et aute mollit et quis eiusmod ea do mollit esse. Ex aliquip ea officia qui deserunt esse amet.\r\n",
        "visited": false
    },
    {
        "_id": "5d8346a97f60629d6ee826b2",
        "index": 5,
        "picture": "http://placehold.it/32x32",
        "age": 26,
        "name": "Figueroa Holland",
        "gender": "male",
        "company": "PRIMORDIA",
        "email": "figueroaholland@primordia.com",
        "phone": "+1 (828) 458-3438",
        "address": "176 Throop Avenue, Nord, Illinois, 3574",
        "about": "Irure duis adipisicing sit est commodo eu cillum tempor enim mollit proident. Ullamco enim enim sunt nisi excepteur ut do ea. Sunt qui dolor eu sit nulla nisi ea reprehenderit sint dolor. Nostrud dolor qui ex nostrud deserunt Lorem anim. Ipsum magna sint enim amet commodo ut labore commodo velit eu non. Veniam minim Lorem deserunt quis officia deserunt cillum reprehenderit deserunt. Excepteur sint officia voluptate proident consequat veniam dolor proident exercitation laborum aliqua non anim.\r\n",
        "visited": false
    }
];

function fixToolbar() {
    $('.action-bar').html('<div class="button-holders">\n' +
        '            <button class="shadow mr-3 bold" id="dislike">\n' +
        '                DisLike\n' +
        '            </button>\n' +
        '            <button class="shadow mr-3 bold" id="super-like">\n' +
        '                Super Like\n' +
        '            </button>\n' +
        '            <button class="bold" id="like">\n' +
        '                Like\n' +
        '            </button>\n' +
        '        </div>').
    css('background','#2196f3');
}

function hello() {
    console.log("hello")
}
function loadNext(iter) {
    console.log(iter)
    if(iter >= 5) {
        toolbarStatus('ff9800', 'You have gone through all matches!', false)
        document.removeEventListener("keydown", toolbarAction);
        console.log("end");
        return ;
    }
    else {
        fixToolbar();
        var index = Math.floor(Math.random()*5);
        console.log(data[index])
        if(data[index].visited )
            loadNext(iter+1);
        else {
            data[index].visited = true;
            let name = data[index].name;
            let worksAt = data[index].company;
            let gender = data[index].gender;
            let about = data[index].about;
            let age = data[index].age;
            $('#name').text(name)
            $('#works-at').text(worksAt);
            $('#about').text(about);
        }
    }

}

function toolbarStatus(color, text, load) {
    $('.action-bar').css('background','#'+color)
        .html('<h3 class="bold">'+text+'</h3>');
    if(load) {
        setTimeout(() => {
            loadNext(0);
        },2000);
    }
}
function toolbarAction(e) {
    switch (e.key) {
        case "ArrowUp":
            toolbarStatus('f44336', 'You super liked!', true);
            break;
        case "ArrowLeft":
            toolbarStatus('ff0000', 'You disliked!', true);
            break;
        case "ArrowRight":
            toolbarStatus('4caf50', 'You liked!', true);
            break;
    }
}



$(document).ready(()=> {
   loadNext(0);
   document.addEventListener("keydown", toolbarAction);
});